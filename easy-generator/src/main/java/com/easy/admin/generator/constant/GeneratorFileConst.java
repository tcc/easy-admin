package com.easy.admin.generator.constant;

/**
 * 生成文件
 *
 * @author TengChongChong
 * @date 2021/1/23
 */
public class GeneratorFileConst {
    /**
     * 实体类
     */
    public static final String MODEL = "model";
    /**
     * dao
     */
    public static final String DAO = "dao";
    /**
     * mapping
     */
    public static final String MAPPING = "mapping";
    /**
     * service
     */
    public static final String SERVICE = "service";
    /**
     * service 实现类
     */
    public static final String SERVICE_IMPL = "serviceImpl";
    /**
     * controller
     */
    public static final String CONTROLLER = "controller";
    /**
     * 列表页
     */
    public static final String LIST_VUE = "List.vue";
    /**
     * 表单页
     */
    public static final String INPUT_VUE = "Input.vue";
    /**
     * 接口文件
     */
    public static final String API_JS = "api.js";
}

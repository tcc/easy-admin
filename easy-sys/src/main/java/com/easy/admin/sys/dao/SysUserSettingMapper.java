package com.easy.admin.sys.dao;

import com.easy.admin.sys.model.SysUserSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户偏好设置
 *
 * @author TengChong
 * @date 2019-03-04 23:41:03
 */
public interface SysUserSettingMapper extends BaseMapper<SysUserSetting> {

}
package com.easy.admin.sys.dao;

import com.easy.admin.sys.model.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统参数
 *
 * @author admin
 * @date 2019-03-03 15:52:44
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}